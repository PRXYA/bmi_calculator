package com.example.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText weight;
    EditText feet;
    EditText inches;
    TextView result1;
    TextView result2;
    Float bmi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Boolean[] valid_weight = {false};
        final Boolean[] valid_feet = {false};
        final Boolean[] valid_inches = {false};

        weight = (EditText) findViewById(R.id.weightInput);
        feet = (EditText) findViewById(R.id.feetInput);
        inches = (EditText) findViewById(R.id.inInput);
        Button calBtn = findViewById(R.id.calcButton);
        result1 = (TextView) findViewById(R.id.resultText);
        result2 = (TextView) findViewById(R.id.otherResultText);


        calBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //checks if weight value is valid
                if(weight.getText().length() ==0){
                    weight.setError("Please enter value");
                }else if(Float.parseFloat(weight.getText().toString()) < 1){
                    weight.setError(("Weight cannot be less than 1lbs"));
                }else valid_weight[0] = true;

                //checks if feet value is valid
                if(feet.getText().length() ==0){
                    feet.setError("Please enter value");
                }else if(Float.parseFloat(feet.getText().toString()) < 1){
                    feet.setError(("Feet cannot be less than 1"));
                }else valid_feet[0] = true;

                //checks if inches value is valid
                if(inches.getText().length() ==0){
                    inches.setError("Please enter value");
                }else if(Float.parseFloat(inches.getText().toString()) < 0){
                    inches.setError(("Inches cannot be negative"));
                }else valid_inches[0] = true;


                if(valid_weight[0]==true && valid_feet[0]==true && valid_inches[0]==true){
                    calculateBMI();
                }else{
                    showToast("Invalid input(s)");
                }

                valid_weight[0] = false;
                valid_feet[0] = false;
                valid_inches[0] = false;

            }
        });
    }

    private void showToast(String toastText) {
        Toast toast=Toast.makeText(getApplicationContext(),toastText,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void calculateBMI() {
        showToast("BMI Calculated.");

        Float weight_val = Float.parseFloat(weight.getText().toString());
        Integer feet_val = Integer.parseInt(feet.getText().toString())*12;
        Integer inches_val = Integer.parseInt(inches.getText().toString());

        bmi = (weight_val/((feet_val+inches_val)*(feet_val+inches_val)))*703;
        String bmi_rounded = String.format("%.1f",bmi); //rounding value to 1 decimal place

        result1.setText("Your BMI: "+bmi_rounded);
        result1.setVisibility(View.VISIBLE);

        if(bmi< 18.5){
            result2.setText("You are Underweight");
        }else if(bmi>=18.5 && bmi <=24.9){
            result2.setText("You have Normal weight");
        }else if(bmi >= 25 && bmi <=29.9){
            result2.setText("You are Overweight");
        }else if(bmi > 30){
            result2.setText("You have Obesity");
        }

        result2.setVisibility(View.VISIBLE);
    }
}
